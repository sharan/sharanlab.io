---
layout: post
title: Test Driven Development in Node.js with Mocha
date: 2019-02-24
---

Test Driven Development (TDD) is a software development process where Tests are written first based on requirement specifications before writing functional code.The developer writes an automated test that defines desired specification first and then writes the functional code to pass the test, one way to think about it is tests are used for specification rather than validation, this has many advantages

*   **Specification**: For developers when writing new code the test specifies the list of features that need to be implemented or acceptance criteria that needs to be met
*   **Refactoring**: Once a test case has passed for a functional unit, we can safely [refactor](https://martinfowler.com/books/refactoring.html) the code with the knowledge that the test cases will have our back, Tests are very vital especially when refactoring other developers code, to ensure that we do not break the functionality. Tests give us the confidence that you can change things without breaking something
*   **Fewer bugs**: The initial cost of TDD may be high when compared to not writing any test, but will produce fewer bugs, in the long run, TDD not only saves time on fixing bugs it also saves time introducing new functionality as the tests act as a safety net that ensures new changes won't break existing functionality
*   **Easier Dependency management**: Tests allows developers to easily change or upgrade external dependencies while making sure the module still works correctly
*   **Documentation**: When tests are written properly, they can be used as documentation for the project
*   **Design**:it makes you to think about the design of the application up front, it gives due consideration to APIs and structure of your code.   Writing the test before the code, makes you think up front of how you will invoke your code and get the result back. This means you design the API _first_ based on how you will use it. This frequently results in a better API.
*   **Quick deployment**: TDD will enable you to release early and often, as you constantly go green in your tests.

[Mocha](https://mochajs.org/) is JavaScript testing framework which runs on Node.js and also in the browser, mocha simplifies synchronous and asynchronous testing. If you are getting started with Test driven development the Mocha is your choice.

In mocha we use `describe` function which is a  [Test Suite](https://en.wikipedia.org/wiki/Test_suite) where we group related tests together:
```javascript
describe("test suite description goes here",function(){
  // individual tests go here
});
```

And then each individual test is defined within a call to the `it` function:
```javascript
describe("test suite description goes here",function(){
  it("test description",function(){
    // your test assertion goes here
  });
});
```

Grouping related tests within `describe` blocks and describing each individual test within an `it` call keeps your tests self documenting.

**Installing Mocha**

Mocha can be installed using Node Package Manager, simply run the command in terminal

```bash
$ npm install -g mocha
```

If you encounter permission denied issue with the previous command then run the same command with sudo
```bash
$ sudo npm install -g mocha
```
Let's write a basic test
```javascript 
const assert = require("assert");

describe("test suite",function(){

    it("test case",function(){
        //assert that 2 is equal to 2
        assert.equal(2,2);
    });

    it("asynchronous test case",function(done){ // done callback for asynchronous testPS1="$ "

        setTimeout(function(){
           // call done callback to complete the test
           done();
       },1000);

    });
});
```

`'it'` will pass the test case if the test does not throw errors, so we have to use assertions to validate and throw errors if our validations fail, in the above example we are using built-in Node.js '**assert**' module, assert will throw errors when assertions fail. For asynchronous tests like HTTP calls or database access we can use done() callback after our asynchronous test has been completed

save the above code to file intro.js and run

```bash
$ mocha intro.js
```

![b1](/assets/b1.jpg)

## Sample Application

Let's Build a sample REST API application for storing and retrieving Employees using TDD approach. we will use MEAN stack to build our sample app. you can download the final source code from [here](https://github.com/sharan01/mochademo) and you can play with the front end app here http://test-mocha.us-west-2.elasticbeanstalk.com/

**Prerequisites**

*   Node 6.11 or greater
*   MongoDB

## **Project Setup**

**Install express-generator**
```bash
$ npm install -g express-generator
```
**Create a new express Project**

```bash
$ express employees
$ cd employees
$ npm install
```

We will use [mongoose](http://mongoosejs.com/) ODM for MongoDB and express-validator to validate input to our APIs, let's install them
```bash
$ npm install --save mongoose express-validator
```
Let's install test dependencies

*   mocha
*   chai
*   chai-http
*   istanbul

```bash
$ npm install --save-dev mocha chai chai-http istanbul
```

[Chai](http://chaijs.com/) is a assertions library that allows us to write [BDD](http://chaijs.com/api/bdd/) style chainable assertions, chai-http is a plugin for chai. We will use chai-http to test our REST APIs and we will use [istanbul](https://istanbul.js.org/) to generate code coverage report

## **Configuring Project for TDD**

Create a config.json file in the conf folder an paste below json
```json
{
    "production" : {
    "DBHost" : "mongodb://127.0.0.1:27017/production",
    "port" : 80
    },
    "development": {
    "DBHost" : "mongodb://127.0.0.1:27017/development",
    "port" : 3000
    },
    "test": {
    "DBHost" : "mongodb://127.0.0.1:27017/test",
    "port" : 4000
    }
}
```
Import config.json file in app.js
```javascript
// get environment variable
const environment = process.env.NODE_ENV '' "development";
const config = require("./conf/config.json")[ environment ];

app.port = config.port;

mongoose.connect(config.DBHost, (err) => {
    if (err) {
        throw err;
    }
});
```
In the above code snippet, the DBHost and port number are determined based on execution environment. In test environment, the config will be the test object from config.json and our tests will run on different port number and connect to different database and will not affect development or production database

and finally, export app module from bin/www file, we will use the app module from our tests
```javascript
module.exports = app;
```
## Writing tests for APIs

our REST API will have 5 endpoints

*   POST /api/employees -> add new employee
*   GET /api/employees -> get all employees
*   GET /api/employees/:id -> get employee by id
*   PUT /api/employees -> update employee
*   DELETE /api/employee/:id -> delete employee by id

**Let's specify rules for the first API POST /api/employees**

*   It should add new employee and return the id of the employee
*   It should return HTTP response status 409 conflict, when employee with email already exists
*   It should return HTTP response status 422 Invalid params when request body contains invalid parameters

We will write test cases based on the above specification, by default mocha looks for test files in 'test' folder in our project, so create a file named 01_POST_employess,js
```javascript
//01_POST_employees.js

// import test dependencies

// chai assertions library
const chai = require("chai");

// chai-http http plugin for accessing rest apis
const chaiHttp = require("chai-http");

// expect is a bdd style assertions from chai
const expect = chai.expect;

// our application server
const server = require("../bin/www");

// use chai-http plugin
chai.use(chaiHttp);

// test suite
describe("POST /api/employees", function() {

    // our sample employee object to add
    const sampleEmployee = {
        name:"max verstappen",
        designation:"Driver",
        email:"max@domain.com"
    };

    // test case
    it("it should add employee and return _id with response status 201",function(done) {

        chai.request(server) // use our application server
            .post("/api/employees") // api path
            .send(sampleEmployee) // request body
            .end((err, response) => { // on response callback
                // expect response to have status 201
                expect(response).to.have.status(201);
                // expect response body to be a json object
                // and to have property named _id
                expect(response.body).to.be.a("object").to.have.property("_id");

                done(); // done callback
            });
    });

    it("it should return 409 conflict, employee already exists",function(done) {

        chai.request(server)
            .post("/api/employees")
            .send(sampleEmployee)
            .end((err, res) => {
                expect(res).to.have.status(409);
                done();
            });
    });

    //invalid input test case
    it("it should return status 422 , invalid params, params missing",function(done) {

        chai.request(server)
            .post("/api/employees")
            .send({ name: "max" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });
    //invalid input test case
    it("it should return status 422 , invalid params, empty name", function(done) {

        chai.request(server)
            .post("/api/employees")
            .send({ name: "" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });

});
```
run the test cases

```bash
$ npm test
```

![Screenshot from 2017-11-15 18-11-44](/assets/screenshot-from-2017-11-15-18-11-44.png)

Our test cases failed as expected, as we have not implemented **POST /api/employees** API yet. Next step is to write** POST /api/empoyees** API, we will not do that here you can get the implementation code from here https://github.com/sharan01/mochademo
After implementing the API let us run the test again

```bash
$ npm test
```

![1success](/assets/1success.png)

All our test cases for **POST /api/employees** have been passed, now let us move to our next API, GET /api/employees, create a file named 02_GET_employees.js
```bash
// 02_GET_employees.js 

const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const server = require("../bin/www");

chai.use(chaiHttp);

// test suite
describe("GET /api/employees", () => {

    it("it should return employees array with size 1",(done) => {

        chai.request(server)
            .get("/api/employees")
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a("array").of.length(1);
                // expect first employee to have these properties
                expect(res.body[0]).to.have.property("_id");
                expect(res.body[0]).to.have.property("name");
                expect(res.body[0]).to.have.property("email");
                expect(res.body[0]).to.have.property("designation");
                done();
            });
    });

});
```
Run test cases

```bash
$ npm test
```

![2fail](/assets/2fail.png)

GET /api/employees test cases failed, now implement the get employees API and run test again

![2success](/assets/2success.png)

Now we will move on to **GET /api/employees/:id** API, in this API we will get employee by id, specify test cases for this API in 03_GET_employees_id.js file
```javascript
const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const server = require("../bin/www");

chai.use(chaiHttp);

describe("GET /api/employees/:id", function(done) {

    let _id;

    const sampleEmployee = {
      name: "Christian Horner",
      designation: "Team Principal",
      email: "christian@domain.com"
    };

    // before hook which run before our test cases in this test suite
    // first we add an employee and get id of that employee
    before(function(done) {

        chai.request(server)
            .post("/api/employees")
            .send(sampleEmployee)
            .end((err, res) => {

                expect(res).to.have.status(201);
                expect(res.body).to.be.a("object").to.have.property("_id");
                // store id for use in below test case
                _id = res.body._id;

                done();
            });

    });

    //
    it("it should return employee inserted in before hook", function(done) {

        chai.request(server)
            .get("/api/employees/"+_id)
            .end((err, res) => {

                expect(res).to.have.status(200);
                // assert returned employee is equal to sample employee
                expect(res.body).to.be.a("object").to.have.property("name").to.be.eq(sampleEmployee.name);
                expect(res.body.designation).to.be.eq(sampleEmployee.designation);
                expect(res.body.email).to.be.eq(sampleEmployee.email);

                done();
            });
    });

    it("it should return status 404 when given invalid id", function(done){

        chai.request(server)
            .get("/api/employees/wrongid")
            .end((err, res) => {

                expect(res).to.have.status(404);

                done();
            });
    });

});
```
you will notice we have used new keyword 'before', before is used in test suite 'describe', 'before' is used when we need any other additional data before running test cases, for out test case we need a valid employee id so we use 'before' hook to add a new employee and get id of that employee to use in our test case. similarly, there is 'after' hook which runs after test cases have been executed, we can use after hook to clean up any data created by test cases

let us run the test again

```bash
$ npm test
```

![3fail](/assets/3fail.png)

GET /api/employees /:id test cases failed, now implement the get employees by id API and run test again

![3success](/assets/3success.png)

You can follow same steps to write test cases and implement APIs for

*   update employee
*   delete employee

Finally, let's run test after implementing above APIs

![Screenshot from 2017-11-15 17-56-10](/assets/screenshot-from-2017-11-15-17-56-10.png)

Great all our tests have passed, now you can further write more test cases to cover all possible edge cases

In the above example we just had 5 APIs, we could have easily tested them with [Postman](https://www.getpostman.com/) and avoid all the extra work, but if you are developing medium to large scale project, then testing each API every time you add or modify functionality is a very time-consuming process, If you follow TDD process then testing each and every API can be done with a single command.

### Code Coverage with Istanbul

Code Coverage is a measurement of how many lines/blocks of your code are executed while the automated tests are running. Code coverage basically tests that how much of your code is covered under tests

Install and run the Istanbul command with mocha

```bash
$ npm install -g istanbul

$ istanbul cover --report html _mocha
```

Istanbul will generate HTML report in our project folder, you can open it in browser

![Screenshot from 2017-11-15 15-27-26](/assets/screenshot-from-2017-11-15-15-27-26.png)

as you can see our tests cover 87% lines of code in the project, you can navigate to individual files to see which lines are not covered under our tests

![Screenshot from 2017-11-15 15-43-12](/assets/screenshot-from-2017-11-15-15-43-12.png)

the lines highlighted in pink were not run during execution of our test cases, we can add new test cases to cover untested code, you can read more about code coverage here [Test Coverage](https://martinfowler.com/bliki/TestCoverage.html)

## Conclusion

In this tutorial, we have used Test driven development approach for building a REST API, although TDD will not make your application completely bug proof, you will certainly catch bugs quickly early in the development process and fix them, instead of catching them in later stages of development process and extending the deadlines

**More Resources**

https://gist.github.com/jbranchaud/60146038904b78f22126

https://www.youtube.com/watch?v=JjqKQ8ezwKQ
